#ifndef     _RUBTINE_H_
#define     _RUBTINE_H_

#include    <stdint.h>
#include    <uuid/uuid.h>

typedef struct coro coro_t;
typedef void (*entry_t)(void *);

// builtin error
typedef struct {
    uuid_t from;
    uuid_t to;
    struct {
        int errcode;
        void *udata;
    } info;
} coro_error_t;


// TODO: error codes

#define     RUBTINE_ERR_NOERR       0       // no more exception
#define     RUBTINE_ERR_USER        1       // user-defined exception
#define     RUBTINE_ERR_NOMEM       2       // ENOMEM caused by swapcontext
#define     RUBTINE_ERR_NOCALLER    3       // dead caller, unable to cede
#define     RUBTINE_ERR_NOCALLEE    4       // dead callee, unable to call
#define     RUBTINE_ERR_WRONGCALL   5       // unmatched caller
#define     RUBTINE_MSG_CALLEERET   6       // callee returns without ceding

// weird sugar for exception handling

#define     _coro_ensure_call(_c_)   \
    do { \
        uint32_t __n_errors_raised__ = rubtine_call(_c_);

        // or

#define     _coro_ensure_cede   \
    do { \
        uint32_t __n_errors_raised__ = rubtine_cede();

        // musts code goes here

#define     _coro_rescue \
        if (__n_errors_raised__ > 0) {

            // rescue code goes here

#define     _coro_end \
        } \
    } while (0)

#define     _coro_errors    __n_errors_raised__

#define     _catch_foreach_begin \
        do { \
            coro_error_t __e__ = rubtine_catch(); \
            if (rubtine_no_error(&__e__)) \
                break; \
            switch (__e__.info.errcode) {
#define     _catch_if_return \
                case RUBTINE_MSG_CALLEERET: {
#define     _catch_if_user_error \
                case RUBTINE_ERR_USER: {
#define     _catch_if_builtin_error \
                default: {
#define     _catch_endif \
                    break; \
                }
#define     _catch_foreach_end \
            } \
        } while (0)

#define     _catched_error  __e__

int rubtine_throw_user(coro_t *c_from, coro_t *c_to, void *u_data);
int rubtine_no_error(coro_error_t *e);
void rubtine_release(coro_t *c);
coro_error_t rubtine_catch(void);
int rubtine_notify(coro_t *c, void *udata);
int rubtine_raise(void *udata);
uint32_t rubtine_cede(void);
uint32_t rubtine_call(coro_t *c);
coro_t *rubtine_new(entry_t u_entry, void *u_arg);
int rubtine_init_thread(uint32_t u_stack_size);
int rubtine_end_thread(void);

#endif      // _RUBTINE_H_
