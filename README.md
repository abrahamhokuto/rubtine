rubtine
=======

A simple and experimental C coroutine library for Linux.
For entertainment purposes mainly.

##Features

* Semi-symmetric coroutine semantics.
* Lightweight coroutine creation - No stack allocated until the coroutine is called.
* Exception mechanism & sugar.

##Experimental Feature
* TODO: Channel Utilities.

##Interface - A Brief Intro

RTFSC is recommended. Details coming soon.

`int rubtine_init_thread(uint32_t u_stack_size);` <br />
Initialize thread-local rubtine runtime. <br />
If `u_stack_size` is smaller than the default stack size, rubtine runtime
will use the default size. <br />

`int rubtine_end_thread(void);` <br />
Release thread-local rubtine runtime. <br />
Only limbo coroutine is destroyed for now. <br />

`coro_t *rubtine_new(entry_t u_entry, void *u_arg);` <br />
Create a new coroutine with entry function `u_entry` and initial argument `u_arg`. <br />

`uint32_t rubtine_call(coro_t *c);` <br />
'Call' semantics. The current coroutine becomes the caller of c. <br />
Only the caller of c is allowed to call c except that c has not been called yet. <br />
`rubtine_call` returns the number of exceptions in the mailbox of the current coroutine. <br />

`uint32_t rubtine_cede(void);` <br />
'Cede' semantics. Yield to the caller of the current coroutine. <br />
Behavior of calling `rubtine_cede` in the initial coroutine is undefined. <br />
`rubtine_cede` returns the number of exceptions in the mailbox of the current coroutine. <br />

`void rubtine_release(coro_t *c);` <br />
Free a coroutine. Usually there is no need to use `rubtine_release` since the coroutine
metadata will be released after it returns. <br />

`int rubtine_raise(void *udata);` <br />
Send a user-defined exception to the caller of the current coroutine. <br />
Behavior of calling `rubtine_raise` in the initial coroutine is undefined. <br />

`int rubtine_notify(coro_t *c, void *udata);` <br />
Send a user-defined exception to coroutine `c`. <br />

`int rubtine_throw_user(coro_t *c_from, coro_t *c_to, void *u_data);` <br />
Send a user-defined exception to coroutine `c_from`, as if it were sent by `c_to`. <br />
Play with it, if you wish. <br />

`coro_error_t rubtine_catch(void);` <br />
Catch an exception in the mailbox of the current coroutine 
and return the information structure. <br />
An exception is not always an error. See below. <br />

`int rubtine_no_error(coro_error_t *e);` <br />
Return 1 if exception structure `e` does not carry any error information. <br />
Usually, `rubtine_catch` and `rubtine_no_error` are not explicitly for there is 
some weird sugar. <br />
