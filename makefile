TARGET=librubtine.so
OBJECTS=rubtine_impl.o chan_impl.o

CFLAGS=-I. -fPIC -shared
LDFLAGS=-luuid

all: $(OBJECTS)
	gcc -o $(TARGET) $(OBJECTS) $(CFLAGS) $(LDFLAGS)

clean:
	rm -f $(TARGET) *.o
