/* Channel Utilities for Rubtine by Hououin Redflag */
#ifndef     _CHAN_IMPL_H_
#define     _CHAN_IMPL_H_

#include "rubtine.h"
#include "stdlib.h"

typedef void* (*rubchan_cft)(void*);

typedef struct
{
	void* p;
	rubchan_cft cf;			/* Copy function */
} rubtine_chan;

/* Error codes */
enum RUBCHAN_ERRCODE{
	CHAN_NOT_EMPTY = -1,
	NOTHING_TO_RECV = -2,
	CFT_RETURN_NULL = -3,
};

void rubchan_new(rubtine_chan*,rubchan_cft);
rubtine_chan* rubchan_alloc(rubchan_cft);
void rubchan_destroy(rubtine_chan*);
int rubchan_send(rubtine_chan*,void*);
void* rubchan_recv(rubtine_chan*);
int rubchan_send_nb(rubtine_chan*,void*);
void* rubchan_recv(rubtine_chan*);

#endif      // _CHAN_IMPL_H_










