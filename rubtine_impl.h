#ifndef     _RUBTINE_IMPL_H_
#define     _RUBTINE_IMPL_H_

#include    <stdint.h>
#include    <ucontext.h>
#include    <uuid/uuid.h>
#include    <util_list.h>
#include    <misc.h>

enum coro_state {
    coro_state_stub,
    coro_state_ready,
    //coro_state_blocking,
    coro_state_dying,
    coro_state_mainflow,
    coro_state_limbo
};

struct coro {
    uuid_t id;
    enum coro_state state;
    struct coro *caller;
    struct {
        ucontext_t cntx;
        void *stack;
        void *arg;
        void (*entry)();
    } self;
    struct {
        list_ctl_t queue;
        uint32_t count;
    } exmailbox;
};


#define     DEF_PAGESIZE        4096
#define     DEF_STACKSIZE       ((4 + 1) * DEF_PAGESIZE)

#endif      // _RUBTINE_IMPL_H_
