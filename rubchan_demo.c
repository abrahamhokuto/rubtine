#include "rubtine.h"
#include "chan_impl.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

rubtine_chan chans[5];

void*
chan_cft(void* str)
{
	char* ret = malloc(strlen((char*)str));
	strcpy(ret,(char*)str);
	return ret;
}
	
void
read_str(void* unused)
{
	(void)unused;
	while(1){
		char buf[1024] = {0};
		for(int i = 0 ; i != 5 ; ++i){
			scanf("%s",buf);
			rubchan_send(&chans[i],(void*)buf);
		}
		rubtine_cede();
	}
}

void
hand_str(void* args)
{
	while(1){
		char* str = rubchan_recv(&chans[(int)args]);
		char* i = str;
		char* j = str + strlen(str) - 1;
	
		int ret = 1;
		
		while(i<j)
			if ( *i++ != *j-- ){
				ret = 0;
				break;
			}
		printf("%s\n",ret?"True":"False");
		free(str);
		rubtine_cede();
	}
}


int
main()
{
	rubtine_init_thread(0);
	coro_t* reader = rubtine_new(read_str,NULL);
	coro_t* handler[5];
	
	for ( unsigned long i = 0 ; i != 5 ; ++i ){
		handler[i] = rubtine_new(hand_str,(void*)i);
		rubchan_new(&chans[i],chan_cft);
	}
	
	while(1){
		rubtine_call(reader);
		for ( int i = 0 ; i != 5 ; ++i )
			rubtine_call(handler[i]);
	}
}
