#ifndef     _MISC_H_
#define     _MISC_H_

#define     unlikely(_x_)       __builtin_expect(!!(_x_), 0)
#define     likely(_x_)         __builtin_expect(!!(_x_), 1)

#define     EXPORT
#define     INNER               static
#define     IMPORT              extern

#endif      // _MISC_H_
