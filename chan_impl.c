#include "chan_impl.h"
#include "rubtine.h"
#include <stdlib.h>

void
rubchan_new(rubtine_chan* chan,rubchan_cft cf)
{
	chan->p = NULL;
	chan->cf = cf;
}

rubtine_chan*
rubchan_alloc(rubchan_cft cf)
{
	rubtine_chan* ret = malloc(sizeof(rubtine_chan));
	rubchan_new(ret,cf);
	return ret;
}

void
rubchan_destroy(rubtine_chan* chan)
{
	free(chan);
}

int
rubchan_send(rubtine_chan* chan,void* object)
{
	/* Block until chan is empty */
	while(chan->p != NULL)
		rubtine_cede();
	
	chan->p = chan->cf(object);

	return chan->p!=NULL?0:CFT_RETURN_NULL;
}

void*
rubchan_recv(rubtine_chan* chan)
{
	/* Block until there's something to receive */
	while( chan->p == NULL )
		rubtine_cede();

	void* ret = chan->p;
	chan->p = NULL;
	return ret;
}

/* Nonblock version */
int
rubchan_send_nb(rubtine_chan* chan,void* object)
{
	if ( chan->p != NULL )
		return CHAN_NOT_EMPTY;

	chan->p = chan->cf(object);
	return chan->p!=NULL?0:CFT_RETURN_NULL;
}

void*
rubchan_recv_nb(rubtine_chan* chan)
{
	if ( chan->p == NULL )
		return (void*)NOTHING_TO_RECV;

	void* ret;
	chan->p = NULL;
	return ret;
}
