#include    <stdint.h>
#include    <stdlib.h>
#include    <rubtine_impl.h>
#include    <rubtine.h>

static __thread coro_t limbo;
static __thread coro_t mainflow, *running = NULL;
static __thread uint32_t stack_size = DEF_STACKSIZE;
static __thread coro_t dummy_caller;


// exception wrapping
typedef struct {
    coro_error_t errinfo;
    list_ctl_t lctl;
} coro_exception_t;

INNER int rubtine_throw_builtin(coro_t *c_from, coro_t *c_to, int u_errcode);

// Context self-collection. Cede to the pseudo-caller.
// XXX: No exception handled. Throw?
INNER
void limbo_loop() {
    while (1) {
        coro_t *c = running;
        running = &limbo;
        limbo.caller = c->caller;
        rubtine_throw_builtin(c, c->caller, RUBTINE_MSG_CALLEERET);
        rubtine_release(c);
        rubtine_cede();
    }
}

// All tmpl and no play makes Jack a dull boy.
#define     make_exception_defn(_o_, _t_, _m_) \
    INNER \
    coro_exception_t *make_exception_##_o_ (uuid_t __u_from__, uuid_t __u_to__, _t_ __u_##_m_) { \
        coro_exception_t *__e__ = (coro_exception_t *) malloc(sizeof(coro_exception_t)); \
        if (unlikely(__e__ == NULL)) \
            return NULL; \
        __e__->errinfo.info._m_ = __u_##_m_; \
        uuid_copy(__e__->errinfo.from, __u_from__); \
        uuid_copy(__e__->errinfo.to, __u_to__); \
        return __e__; \
    }

// Semicolons, just to make the editor silent.
make_exception_defn(builtin, int, errcode);
make_exception_defn(user, void *, udata);

// XXX: Keep this hand-written implementation for further modification.
INNER
int rubtine_throw_builtin(coro_t *c_from, coro_t *c_to, int u_errcode) {
    int ret = 0;
    coro_exception_t *e = make_exception_builtin(c_from->id, c_to->id, u_errcode);
    if (e != NULL) {
        list_add_tail(&(e->lctl), &(c_to->exmailbox.queue));
        c_to->exmailbox.count++;
    } else
        ret = -1;
    return ret;
}

// Manually throw a user-defined exception.
// Exceptions are not messages - Use them only when necessary.
EXPORT
int rubtine_throw_user(coro_t *c_from, coro_t *c_to, void *u_data) {
    int ret = 0;
    coro_exception_t *e = make_exception_user(c_from->id, c_to->id, u_data);
    e->errinfo.info.errcode = RUBTINE_ERR_USER;
    if (e != NULL) {
        list_add_tail(&(e->lctl), &(c_to->exmailbox.queue));
        c_to->exmailbox.count++;
    } else
        ret = -1;
    return ret;
}

EXPORT inline
int rubtine_no_error(coro_error_t *e) {
    return uuid_is_null(e->to);
}

EXPORT inline
void rubtine_release(coro_t *c) {
    if (c->self.stack != NULL)
        free(c->self.stack);
    free(c);
}

// Catch an exception from coroutine mailbox.
EXPORT
coro_error_t rubtine_catch(void) {
    coro_error_t err;

    if (unlikely(running->exmailbox.count == 0)) {
        uuid_clear(err.to);
        return err;
    }

    list_ctl_t *l = running->exmailbox.queue.next;
    list_del(l);
    running->exmailbox.count--;

    coro_exception_t *e = container_of(l, coro_exception_t, lctl);
    err.info = e->errinfo.info;
    uuid_copy(err.from, e->errinfo.from);
    uuid_copy(err.to, e->errinfo.to);
    free(e);

    return err;
}

// Throw an exception, which will be detected in coroutine c later.
EXPORT inline
int rubtine_notify(coro_t *c, void *udata) {
    rubtine_throw_user(running, c, udata);
    return 0;
}

// Raise an exception, which will be detected in caller later.
EXPORT inline
int rubtine_raise(void *udata) {
    rubtine_throw_user(running, running->caller, udata);
    return 0;
}

// XXX: check caller
// Yield to caller of the current coroutine.
// Return the number of caller's exceptions by the time caller decide to crash.
// If -1 is returned, coroutine must quit gracefully.
EXPORT
uint32_t rubtine_cede(void) {
    coro_t *l = running;
    running = l->caller;

    if (unlikely(swapcontext(&(l->self.cntx), &(running->self.cntx)) == -1)) {
        running = l;
        rubtine_throw_builtin(l, l, RUBTINE_ERR_NOMEM);
    }

    return l->exmailbox.count;
}

// XXX: check callee
// Call a coroutine, which goes on executing from its last breakpoint.
// A stub coroutine executes from the function entry, and the current 
// routine/coroutine becomes caller of parameter c, as a side effect.
// Return the number of callee's exceptions by the time callee gracefully crashed.
EXPORT
uint32_t rubtine_call(coro_t *c) {
    // XXX: unlikely condition?
    if (c->state == coro_state_stub) {
        // bind caller
        c->caller = running;
        // generate uuid
        uuid_generate(c->id);
        // allocate stack
        c->self.stack = malloc((size_t) stack_size);
        getcontext(&(c->self.cntx));
        c->self.cntx.uc_stack.ss_sp = c->self.stack;
        c->self.cntx.uc_stack.ss_size = (size_t) stack_size;
        // set successor context to limbo
        c->self.cntx.uc_link = &limbo.self.cntx;
        // make context
        makecontext(&(c->self.cntx), c->self.entry, 1, c->self.arg);
        // state change
        c->state = coro_state_ready;
    } else {
        if (unlikely(uuid_compare(c->caller->id, running->id) != 0)) {
            rubtine_throw_builtin(running, running, RUBTINE_ERR_WRONGCALL);
            return running->exmailbox.count;
        }
    }
    // change running coro and swap context
    coro_t *l = running;
    running = c;

    if (unlikely(swapcontext(&(l->self.cntx), &(c->self.cntx)) == -1)) {
        running = l;
        rubtine_throw_builtin(l, l, RUBTINE_ERR_NOMEM);
    }

    // Any switching back causes running to be changed to l again. Trust running.
    return l->exmailbox.count;
}

// Create a stub coroutine with its caller and context uninitialized.
EXPORT
coro_t *rubtine_new(entry_t u_entry, void *u_arg) {
    coro_t *c = (coro_t *) malloc(sizeof(coro_t));
    
    if (unlikely(c == NULL))
        goto brk;
    c->state = coro_state_stub;
    c->caller = NULL;
    c->self.stack = NULL;
    c->self.arg = u_arg;
    c->self.entry = u_entry;
    c->exmailbox.count = 0;
    init_list_head(&(c->exmailbox.queue));
brk:
    return c;
}

// Thread local ctor for rubtine runtime.
// Must be called in each thread which uses rubtine.
// Return 0 if success, -1 otherwise.
EXPORT
int rubtine_init_thread(uint32_t u_stack_size) {
    static int init_reentry = 0;
    if (unlikely(init_reentry == 0))
        if (u_stack_size > DEF_STACKSIZE)
            stack_size = u_stack_size;

    // mainflow initialization
    uuid_generate(mainflow.id);
    mainflow.state = coro_state_mainflow;
    mainflow.caller = &dummy_caller;
    mainflow.self.stack = NULL;
    mainflow.exmailbox.count = 0;
    init_list_head(&(mainflow.exmailbox.queue));
    running = &mainflow;

    // limbo initialization
    uuid_generate(limbo.id);
    limbo.state = coro_state_limbo;
    limbo.caller = &dummy_caller;
    limbo.self.stack = malloc((size_t) stack_size);
    limbo.self.entry = limbo_loop;
    limbo.exmailbox.count = 0;
    init_list_head(&(limbo.exmailbox.queue));

    // limbo context initialization
    getcontext(&(limbo.self.cntx));
    limbo.self.cntx.uc_stack.ss_sp = limbo.self.stack;
    limbo.self.cntx.uc_stack.ss_size = (size_t) stack_size;
    makecontext(&(limbo.self.cntx), limbo.self.entry, 0);

    // XXX: invocation graph initialization

    return 0;
}

// Thread local dtor for rubtine runtime.
// Must be called in each thread which uses rubtine.
// Return 0 if success, -1 otherwise.
// Mind your own exceptions.
EXPORT
int rubtine_end_thread(void) {
    // XXX: free invocation graph
    free(limbo.self.stack);
    return 0;
}
